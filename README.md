# HotkeyCloser

Tiny autohotkey program that launches the program specified in the arguments, and closes the program and itself when a hotkey is pressed.

# Usage

Build this by right clicking `launcher.ahk` and hitting "Compile script".

Launching the program with an argument will launch the argument and wait for a specified hotkey to close the launched program and itself.

```launcher.exe C:/emu/Dolphin/dolphin.exe -b -e "C:/Animal Crossing.iso"```

By default, it listens for the `start` and `select` (or `back`) buttons on any connected xinput gamepad. It is trivial to change the source to use your own combination.