#Include xinput.ahk

Gui, Color, black

Gui -Caption

Gui, Show, x0 y0 w%A_ScreenWidth% h%A_ScreenHeight%

NormalizePath(path) {
    ; normalizes a path
    ; changes slashes to windows separaters
    cc := DllCall("GetFullPathName", "str", path, "uint", 0, "ptr", 0, "ptr", 0, "uint")
    VarSetCapacity(buf, cc*2)
    DllCall("GetFullPathName", "str", path, "uint", cc, "str", buf, "ptr", 0)
    return buf
}

GetChildProcess(ProcID) {
    wmi := ComObjGet("winmgmts:")
    queryEnum := wmi.ExecQuery("SELECT * FROM Win32_Process WHERE ParentProcessId=" . pid)
    for proc in queryExc {
        MsgBox % proc.Name
        return proc.ProcessID
    }
    return 0
}

KillChildProcesses(ProcID) {
    MsgBox % ProcID
    wmi := ComObjGet("winmgmts:")
    queryEnum := wmi.ExecQuery("SELECT * FROM Win32_Process WHERE ParentProcessId=" . pid)
    for proc in queryExc {
        MsgBox % proc.Name
        KillChildProcesses(proc.ProcessID)
        WinKill, ahk_pid proc.ProcessID,,2
    }
    return True
}

execString := ""
for n, param in A_Args {
    ;only add arguments to the execstring, as programname is determined after this.
    if n > 1 
    {
        IfInString,param,%A_Space%
            param="%param%"
        execString = %execString% %param%
    }
}

programFull := A_Args[1]

programFull := NormalizePath(programFull) ; normalize the path

; split the path into the name and path.
SplitPath, programFull, programName, BaseDir

; debug - display full, name, and dir
;MsgBox, %programFull% : %programName% : %BaseDir%

; run the program with the arguments provided, and set the working dir to the program's directory
Run, "%programFull%" %execString%, %BaseDir%, UseErrorLevel, PID

WaitToHook := false

Needle := ".url"

if InStr(programName, Needle)
    PID := 0

; set the program's priority to high because why not
if PID
    Process, Priority, %PID%, High

; loop through xinput controllers until we close the program
Loop {
    Loop, 4 {
        if State := XInput_GetState(A_Index-1) {
            ; check if the 'back' and 'start' buttons are both being held down
            if XInput_bIsDown("Back", State.wButtons) & XInput_bIsDown("Start", State.wButtons) {
                if !PID
                    WinGet, PID, PID, A

                ;KillChildProcesses(PID)
                WinKill, ahk_pid %PID%,,15 ; attempt graceful shutdown of process, or instakill after 15seconds
                ; does winkill actually attempt process close after 15 seconds? in my tests it would just do nothing (pcsx2 misconfigured, staying open)
                Process, Close, %PID% ; ehh let's see
                ExitApp ; exit this script
            }
        }

        Sleep, 0 ; give the OS a chance to breathe
    }

    Sleep, 0

    ; check if process still exists
    if PID
    {
        Process, Exist, %PID%

        ; if not, close script
        if !errorlevel
            ExitApp
    }
    else
    {
        Sleep, 50
        WinGet, Name, ProcessName

        ; die if we don't have a PID and current exe is emulationstation
        ; even though it shouldn't ever show up until this script has closed.
        ; need better solution to figure out what is intended to be 'currently running' and track when its closed
        ; when not closed by this script.
        if InStr(Name, "emulationstation")
            ExitApp
        Sleep, 0
    }
}

